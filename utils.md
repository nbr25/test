### Command to backup a system partition 

`sudo dd if=/dev/sdb3 conv=sync,noerror bs=64k status=progress | gzip -c > /home/nbr/test.img.gz`

---

### Command to copy with rsync with progress

`rsync --info=progress2 -auvzh test.img.gz /directory`

---

### Command to get all block ip from ufw

`blockip() {
	cat /var/log/ufw.log | awk '{print $12}' | sed 's/SRC=//' | sort -u
}`

---

### Output script without comments

`clean() {cat $1 | egrep -v "^\s*(#|$)"}`

---

### To search all file with SUID:

`find / -user root -perm -4000 -exec ls -ldb {} \;`

---

### To create a log file of all typed commands on shell

`$(ps -ocommand= -p $PPID | awk '{print $1}') == 'script' || (script -f "$HOME"/"$(date +"%d-%b-%y_%H-%M-%S")"_shell.log)`

---